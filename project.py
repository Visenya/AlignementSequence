#! /usr/bin/env python3

"""
    Importations of different libraries.
        * BioPython
        * Networkx
        * Matplotlib
        * Tkinter
        * Regex
"""

#######################
#      Liste Def      #
#######################

def testIfAdn(seq):
    """
        Test if the base in sequence is ADN

        @return bool
    """
    return bool(re.match("^[ACGT]*$", seq.upper()))


def loadData(nameFile):
    """
        Load data

        @return record_dict : dictionnary
    """
    record_dict = SeqIO.to_dict(SeqIO.parse(nameFile, "fasta"))
    for value in record_dict.values() :
        if (not testIfAdn(str(value.seq))):
            raise Exception()
    return record_dict


def showData(scores):
    """
        Print percentage of differences between two known ADN sequences
        Not used in the application, had allowed to check if the score between two sequences is correct

        @param scores : dictionnary
    """
    seq1 = input('Séquence 1 ?')
    seq2 = input('Séquence 2 ?')
    if seq1 in scores[seq2]:
        res = scores[seq2][seq1]
    else:
        res = scores[seq1][seq2]
    print(round(res, 2) * 100, '% de différence')


def formatDataFasta(nameFile = "aze.fa"):
    """
        Creating the variable in which data is stored

        @return scores : dictionnary
    """

    try :
        data = loadData(nameFile)
    except Exception :
        raise Exception
    scores = {}

    for key, val in data.items():
        scores[key] = {}
        for key2, val2 in data.items():
            if (key2 == key):
                break
            if key in scores[key2]:
                break
            alignements = pairwise2.align.globalxx(val.seq, val2.seq)
            maxlen = max(len(val.seq), len(val2.seq))
            scores[key][key2] = (maxlen - alignements[0][2]) / maxlen
    return scores


def buildGraph(scores, rate = 0.30) :
    """
        Build and Show a Graph

        @param scores : dictionnary
        @param rate : float
    """
    # Dots created and calculated by networkx
    plt.clf()
    G = nx.Graph()
    G.add_nodes_from(scores.keys())
    bridges = []
    labelsEdges = {}

    for keys in scores:
        for keys2 in scores:
            if keys2 in scores[keys] and scores[keys][keys2] <= float(rate):
                G.add_edge(keys, keys2)
                labelsEdges[(keys,keys2)] = round(scores[keys][keys2], 2)

    pos = nx.nx_agraph.graphviz_layout(G)
    nx.draw_networkx_nodes(G,pos, node_color='r',node_size=500,alpha=0.8)
    nx.draw_networkx_edges(G,pos)

    labels = {}

    for key in scores.keys():
        labels[key] = key

    nx.draw_networkx_edge_labels(G, pos, labelsEdges)
    nx.draw_networkx_labels(G, pos, labels, font_size=12)

    # Display with plt
    font = {'fontname'   : 'Arial',
               'color'      : 'k',
               'fontweight' : 'bold',
               'fontsize'   : 14}
    plt.title("Comparaison de Séquence ADN dans le fichier FASTA", font)
    plt.axis('off')
    plt.show()


def boutonParcourir():
    """
        Launch the search of file fasta

    """
    global fileNameGlobal
    global scores

    try :
        filename = filedialog.askopenfilename(title = "Ouvrir votre fichier", filetypes = [('fasta files','.fa'),('fasta files','.Fasta'),('fasta files','.fasta')])

        fichier = open(filename, "r")
        fichier.close()

        record_dict = SeqIO.to_dict(SeqIO.parse(filename, "fasta"))
        fileNameGlobal = filename

        for child in fenetre.winfo_children():
            if isinstance(child, Listbox) or isinstance(child, Scrollbar):
                child.destroy()

        # Scrollbar
        xscrollbar = Scrollbar(fenetre, orient = HORIZONTAL)
        xscrollbar.pack(side = BOTTOM, fill = X)

        yscrollbar = Scrollbar(fenetre)
        yscrollbar.pack(side = RIGHT, fill = Y)

        listbox = Listbox(fenetre, xscrollcommand = xscrollbar.set, yscrollcommand = yscrollbar.set)
        listbox.configure(width = 150, height = 50)
        listbox.pack()
        for key, val in record_dict.items():
            listbox.insert(END, str(key), str(val.seq))

        xscrollbar.config(command = listbox.xview)
        yscrollbar.config(command = listbox.yview)

        if isinstance(scores, dict) :
            try :
                scores = formatDataFasta(fileNameGlobal)
            except Exception :
                messagebox.showinfo("Attention", "Au moins une des séquences présent dans ce fichier contient des bases nucleiques autre que celles constituant l'ADN, c'est-à-dire A, C, T et G.", icon = 'warning')

    except TypeError:
        messagebox.showinfo("Erreur",
                            "Aucun Fichier selectionné.",
                            icon='error')


def recName(inputRate) :
    """
        Store the input value in a variable

        @param inputRate : object

    """
    global rate
    if float(inputRate.get()) > 0 and float(inputRate.get()) < 1.0 :
        rate = inputRate.get()
    else :
        messagebox.showinfo("Attention", "Le seuil doit être compris entre 0 et 1.0.", icon = 'error')


def toggleInputRate() :
    """
        Hide or show the input

    """
    global hidden
    global rate

    for child in frameRate.winfo_children():
        child.destroy()

    if hidden :
        newRate = DoubleVar()
        labelRate = Label(frameRate, text = "Le seuil est de : %s.\nTouche Entrée pour valider." %(rate))
        inputRate = Entry(frameRate, textvariable = newRate, bd = 5)
        inputRate.pack()
        inputRate.bind("<Return>", lambda event : recName(inputRate))

        labelRate.pack()
        inputRate.pack()
        hidden = False
    else :
        hidden = True
        labelRate = Label(frameRate, text = "Le seuil est de : %s " %(rate))
        labelRate.pack()


try:
    import re
    import string
    import numpy as np
    from Bio import pairwise2
    from Bio.pairwise2 import format_alignment
    from Bio import SeqIO
    from tkinter import *
    from tkinter.filedialog import *
    from tkinter import filedialog
    from tkinter import messagebox
    from tkinter.messagebox import showerror
    import networkx as nx
    import matplotlib.pyplot as plt


    #########################
    #      Application      #
    #########################

    fenetre = Tk()
    fenetre.title('Project DNA')
    fenetre['bg'] = 'lightgrey' # couleur de fond


    ###############################
    #      Gestion des Frame      #
    ###############################

    frameContent = Frame(fenetre, borderwidth = 2, relief = GROOVE)
    frameContent.pack(side = "top", padx = 20, pady = 20)
    frameButton = Frame(fenetre, borderwidth = 2, relief = GROOVE)
    frameButton.pack(side = "bottom", padx = 20, pady = 20)

    frame1 = Frame(frameButton, borderwidth = 2, relief = GROOVE)
    frame1.pack(side = "top", padx = 20, pady = 10)
    frame2 = Frame(frameButton, borderwidth = 2, relief = GROOVE)
    frame2.pack(side = "bottom", padx = 20, pady = 10)

    frameRate = Frame(frame2, borderwidth = 2, relief = GROOVE)
    frameRate.pack(side = "top", padx = 20, pady = 10)

    fenetre.geometry("1400x1000+300+0")


    ##############################
    #      Variable Globale      #
    ##############################

    fileNameGlobal = "aze.fa"
    scores = {}
    rate = 0.3
    hidden = True


    #################################
    #      Gestion des Boutons      #
    #################################

    label = Label(frame1, text = "Alignement de séquence")
    label.pack(side = "top", padx = 10, pady = 10)
    buttonLoop = Button(frame1, text="Parcourir fichier fasta", command = boutonParcourir)
    buttonLoop.pack(side = "bottom", padx = 10, pady = 10, anchor="w")

    buttonGraphe = Button(frame2, text = "Graph", command = lambda : buildGraph(scores, rate))
    buttonGraphe.pack(side = LEFT, padx = 10, pady = 10, anchor="w")

    buttonRate = Button(frame2, text = "Modifier seuil", command = toggleInputRate)
    buttonRate.pack(side = LEFT, padx = 10, pady = 10, anchor="w")

    fenetre.mainloop()

except ImportError:
    print("Une erreur s'est produite lors de l'importation des modules. \nIl est possible que certains modules ne soient pas installés dans votre environemment. \nMerci de consulter le README.")
