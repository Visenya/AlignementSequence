Projet d'Alignement de Séquence Graphique
===

##### Réalisé dans le cadre du module 'Programation de script pour les biologistes'
##### Professeur : EVEILLARD Damien
##### Auteur : _CHETTA Aline_ et _NAULLET Guillaume_


## But de l'application :

Cette application permet de charger un fichier de type _fasta_ contenant des séquences d'ADN. Ces différentes séquences d'ADN sont ensuite alignées une à une et un score est calculé à partir de cet alignement. Au final, un graphique est généré. Ainsi, chaque nœud correspond à une séquence, les ponts générés entre deux séquences sont dépendants d'un seuil de score.

## Lancer l'application :

    # Pré-requis :
      pip3 install Biopython
      pip3 install Biopython --upgrade
      pip3 install networkx
      pip3 install matplotlib
      sudo apt install python3-tk
      sudo apt install build-essential
      sudo apt install graphviz
      pip3 install pygraphviz
      pip3 install graphviz

    # Exécuter l'application :
      cd `cheminDuProjet`
      python3 project.py


## Fonctionnement :

Tout d'abord afin de faciliter l'utilisation du programme, l'interface utilisant la bibliothèque _TKinter_ a été mise en place. Cette interface comporte deux parties principales :
 * la partie supérieure permettant de montrer le contenu du fichier _fasta_,
 * et la partie inférieure dédiée au menu d'utilisation. Elle permet de rechercher le fichier _fasta_ par l'intermédiaire du bouton parcourir, de modifier le seuil de tracabilité des ponts et d'éxécuter le graphique.

La première étape est de sélectionner le fichier _fasta_. Une fois le fichier _fasta_ chargé, un test est effectué pour vérifier que la séquence est bien une séquence ADN. Il est possible dès lors de générer un graphique selon un seuil de 0.3 par défaut. Si un autre seuil est désiré, il possible de modifier le seuil. `Attention pour que le nouveau seuil soit bien pris en compte, la saisie doit être validée par la touche "Retour charriot" du clavier. Pour constater le changement, il est nécessaire d'appuyer de nouveau sur le bouton « Modifier seuil ». `
